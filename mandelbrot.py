import numpy as np
import matplotlib.pyplot as plt

MAX_ITERATIONS = 100

def iterations(c):
    z = 0
    i = 0

    while abs(z) < 4 and i < MAX_ITERATIONS:
        z = z**2 + c
        i += 1

    if (i == MAX_ITERATIONS):
        return 0
    
    return i

scale = 100

x_min = -2
x_max = 2

y_min = -2
y_max = 2

xs = np.linspace(x_min, x_max, 1000)
ys = np.linspace(y_min, y_max, 1000)

plane = np.empty((1000, 1000))

for idx_x in range(len(xs)):
    for idx_y in range(len(ys)):
        c = complex(xs[idx_x], ys[idx_y])
        plane[idx_x, idx_y] = iterations(c)

plt.imshow(plane.T, cmap='hot', extent=[x_min, x_max, y_min, y_max])
plt.colorbar()
plt.xlabel(r"$\Re$", fontsize=20)
plt.ylabel(r"$\Im$", fontsize=20)
plt.show()